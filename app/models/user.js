var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({

   
    local            : {
        user       : String,
        password     : String,
    }

});

userSchema.methods = {
// methods ======================
// generating a hash
generateHash : function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
},

// checking if password is valid
validPassword : function(password) {
    return bcrypt.compareSync(password, this.local.password);
}

};
module.exports = mongoose.model('User', userSchema);
